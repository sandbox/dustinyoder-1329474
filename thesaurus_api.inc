<?php

function _thesaurus_api_test() {
  $result = _thesaurus_api_get_synonyms("shock");
	if($result == null) {
		echo "error reading file or finding word";
	} else {
		print_r( $result );
	}
}



/**
 * api function which takes a string word or phrase as an argument
 * returns an array of synonyms for that word or phrase from the thesuarus library.
 */
function _thesaurus_api_get_synonyms($search) {
  $synonyms = array();

  $idxfile = _thesaurus_api_open_file('idx');
	$datfile = _thesaurus_api_open_file('dat');
	
	if($idxfile == null || $datfile == null) {
		return null;
	}
	
	$dat_offset = _thesaurus_api_locate_entry($search, $idxfile);
	
	if($dat_offset == null) {
		return null;
	}

	$rows = _thesaurus_api_lookup_entry($dat_offset, $datfile);

  foreach($rows as $row) {
		$words = explode('|', $row);
		for($i=1; $i<count($words); $i++) {  //for each word skipping the first
      $word = trim(preg_replace('/\((.*?)\)/', '', $words[$i]));
			$synonyms[$word] = $word;
		}
	}
	
	return $synonyms;
}





/**
 * Return the path to the thesuarus_api library folder.
 */
function _thesaurus_api_get_library_path() {
  return libraries_get_path('thesaurus_api');
}





/**
 * Return the file id to the open idx or dat file of the thesaurus library
 */
function _thesaurus_api_open_file($filetype) {
  $path = _thesaurus_api_get_library_path();
  if(!$path) {
    drupal_set_message('Path to thesaurus library files not found.  Copy .dat and .idx files to thesaurus_api folder to your libraries folder.', 'warning');
		return null;
	}
	
	//use reg expression to find the first file in library folder with matching extension
	$regex = "/." . $filetype . "/";
	$dirobj = current(file_scan_directory($path, $regex));

  $realpath = drupal_realpath($dirobj->uri);

	$handle = fopen($realpath, "r");

	if(!$handle) {
		return null;
	}
	
  return $handle;
}






/**
 * Locates a word in the library .idx thesuarus file using the string parameter of our search target
 * returns the byte offset of this word or phrase in the dat file.
 */
function _thesaurus_api_locate_entry($search, $idxfile) {
	$search = strtolower($search);

  $encoding = fgets($idxfile);
  $min = 0;
	$max = fstat($idxfile);  //get file stat ojbect
	$max = $max['size'];  //replace object with just the filesize in bytes
  $mid = floor($max / 2);  //find the midpoint

  do {
    fseek($idxfile, $mid);
    $cur_line = fgets($idxfile);  //read to an end of line char
    $cur_line = fgets($idxfile);  //read the next full line
	
    $cur_line = explode('|', $cur_line);

    $diff = strcmp($cur_line[0], $search);
	
    if($diff > 0) {
      $max = $mid;
      $mid = floor(($max - $min) / 2) + $min;
    }
    if($diff < 0) {
      $min = $mid;
      $mid = floor(($max - $min) / 2) + $min;
    }
		if($diff == 0) {  //found match
			return $cur_line[1];  //return the offset for this word.
		}
  } while(($min != $mid) && ($max != $mid));

  return "not found";
}





/**
 * Looks up a word in the dat file using the dat file offset that should have been
 * found in the idx file.  Returns array of rows for this entry from the dat file
 */
function _thesaurus_api_lookup_entry($dat_offset, $datfile) {
	$format = fgets($datfile);
	fseek($datfile, $dat_offset);

  $rows = array();
  $entry = explode('|', fgets($datfile));
	for($i=0; $i<$entry[1]; $i++) {
		$rows[] = fgets($datfile);
	}
  return $rows;
}

?>